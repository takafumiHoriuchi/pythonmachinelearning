import os
import struct
import numpy as np

def load_mnist(path, kind='train'):
    # パスを設定
    labels_path = os.path.join(path, '%s-labels.idx1-ubyte' % kind)
    images_path = os.path.join(path, '%s-images.idx3-ubyte' % kind)

    # rb：読み込みのバイナリモード
    with open(labels_path, 'rb') as lbpath:
        # バイナリを文字列に変換（magic-numberはファイルのプロトコル）
        magic, n = struct.unpack('>II', lbpath.read(8))
        # 配列を構築
        labels = np.fromfile(lbpath, dtype=np.uint8)

    with open(images_path, 'rb') as imgpath:
        magic, num, rows, cols = struct.unpack(">IIII", imgpath.read(16))
        # 配列のサイズも変更
        images = np.fromfile(imgpath, dtype=np.uint8).reshape(len(labels), 784)

    return images, labels


X_train, y_train = load_mnist('mnist', kind="train")
print('Rows: %d, columns: %d' % (X_train.shape[0], X_train.shape[1]))
X_test, y_test = load_mnist('mnist', kind="t10k")
print('Rows: %d, columns: %d' % (X_test.shape[0], X_test.shape[1]))