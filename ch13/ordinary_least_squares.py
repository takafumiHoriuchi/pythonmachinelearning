# 最小二乗法 線形回帰分析

# データセットを作成
X_train = np.asarray([[0.0], [1.0],
                     [2.0], [3.0],
                     [4.0], [5.0],
                     [6.0], [7.0],
                     [8.0], [9.0]],
                     dtype=theano.config.floatX)
y_train = np.asarray([1.0, 1.3,
                      3.1, 2.0,
                      5.0, 6.3,
                      6.6, 7.4,
                      8.0, 9.0],
                      dtype=theano.config.floatX)


import theano
from theano import tensor as T
import numpy as np


# 線形回帰モデルの重みを学習
def train_linreg(X_train, y_train, eta, epochs):

    costs = []

    # 配列の初期化
    eta0 = T.fscalar('eta0')
    y = T.fvector(name='y')
    X = T.fmatrix(name='X')
    # 重みを作成
    w = theano.shared(np.zeros(shape=(X_train.shape[1] + 1),
                               dtype=theano.config.floatX),
                               name='w')
    # コストの計算
    # 総入力を計算
    net_input = T.dot(X, w[1:]) + w[0]
    # w[0]はバイアスユニット。つまり、x=0でのy軸の切片
    errors = y - net_input
    # 教師ラベル(y)と予測(net_input)の誤差
    cost = T.sum(T.pow(errors, 2))
    # 誤差の二乗和
    # このcostが最小になるようにトレーニングする

    # 重みの更新
    # wrtパラメータに渡された引数を自動的に微分する
    gradient = T.grad(cost, wrt=w)
    # コストの勾配(微分)
    update = [(w, w - eta0 * gradient)]
    # コストの勾配に学習率を掛ける
    # 重みを更新

    # モデルのコンパル
    train = theano.function(inputs=[eta0],
                            outputs=cost,
                            updates=update,
                            givens={X: X_train, y: y_train})

    # '_'は、イテレーションが回数分だけ行われることを示すために利用される
    for _ in range(epochs):
        costs.append(train(eta))

    return costs, w


import matplotlib.pyplot as plt
costs, w = train_linreg(X_train, y_train, eta=0.001, epochs=10)
plt.plot(range(1, len(costs)+1), costs)
plt.xlabel('Epoch')
plt.ylabel('Cost')
plt.tight_layout()
plt.show()