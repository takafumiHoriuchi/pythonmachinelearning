import numpy as np

# 初期化
x = T.fmatrix(name='x')
x_sum = T.sum(x, axis=0)

# コンパイル
calc_sum = theano.function(inputs=[x], outputs=x_sum)

# 実行（Pythonリスト）
ary = [[1,2,3],[1,2,3]]
print('Column sum:', calc_sum(ary))

# 実行（NumPy配列）
ary = np.array([[1,2,3],[1,2,3]], dtype=theano.config.floatX)
print('Column sum:', calc_sum(ary))
