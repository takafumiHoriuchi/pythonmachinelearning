"""
# movies data
import pandas as pd
df = pd.read_csv('./movie_data.csv')

import re
def preprocessor(text):
    text = re.sub('<[^>]*>', '', text)
    emoticons = re.findall('(?::|;|=)(?:-)?(?:\)|\(|D|P)', text)
    text = re.sub('[\W]+', ' ', text.lower()) + ' '.join(emoticons).replace('-', '')
    # text = re.sub('[\W]+', ' ', text.lower()) + ''.join(emoticons).replace('-', '')
    return text

df['review'] = df['review'].apply(preprocessor)
"""

# ドキュメントをトークン化する
def tokenizer(text):
    return text.split()

print(tokenizer('runners like running and thus they run'))
# output: ['runners', 'like', 'running', 'and', 'thus', 'they', 'run']

# ワードステミング：単語を原型に変換する
# pip istall nltk でNLTK(Natural Language Toolkit for Python)をインストールする
from nltk.stem.porter import PorterStemmer
porter = PorterStemmer()
def tokenizer_porter(text):
    return [porter.stem(word) for word in text.split()]

print(tokenizer_porter('runners like running and thus they run'))
# output: ['runner', 'like', 'run', 'and', 'thu', 'they', 'run']

# stop-word removal
import nltk
# nltk.download('stopwords')

from nltk.corpus import stopwords
stop = stopwords.words('english')
print([w for w in tokenizer_porter('a runner likes running and runs a lot')[-10:] if w not in stop])
# output: ['runner', 'like', 'run', 'run', 'lot']
