X_train, y_train = load_mnist('.', kind="train")
print('Rows: %d, columns: %d' % (X_train.shape[0], X_train.shape[1]))

X_test, y_test = load_mnist('.', kind="t10k")
print('Rows: %d, columns: %d' % (X_test.shape[0], X_test.shape[1]))