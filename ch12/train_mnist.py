import theano

# MNISTの画像配列を32ビット形式に変関する
theano.config.floatX = 'float32'
X_train = X_train.astype(theano.config.floatX)
X_test = X_test.astype(theano.config.floatX)

# クラスラベルをone-hotフォーマットに変換
from keras.utils import np_utils
print('First 3 labels: ', y_train[:3])
y_train_ohe = np_utils.to_categorical(y_train)
print('\nFirst 3 labels (one-hot):\n', y_train_ohe[:3])

########################################################

# ニューラルネットワークを実装する
from keras.model import Sequential
from keras.layers.core import Dense
from keras.optimizers import SGD

np.random.seed(1)

# モデルを初期化
model = Sequential()

# 隠れ層１
model.add(Dense(input_dim=X_train.shape[1],
                output_dim=50,
                init='uniform',
                activation='tanh'))

# 隠れ層２
model.add(Dense(input_dim=50,
                output_dim=50,
                init='uniform',
                activation='tanh'))

# 出力層
model.add(Dense(input_dim=50,
                output_dim=y_train_ohe.shape[1],
                init='uniform',
                activation='softmax'))

# モデルコンパイル時のオプティマイザを設定
sgd = SGD(lr=0.001, decay=1e-7, momentum=.9)
# モデルをコンパイル
model.compile(loss='categorical_crossentropy',
              optimizer='sgd',
              metrics=['accuracy'])


########################################################

model.fit(X_train,
          y_train_ohe,
          nb_epoch=50,
          batch_size=300,
          verbose=1,
          validation_split=0.1)

########################################################

# クラスラベルを予測する

y_train_pred = model.predict_classes(X_train, verbose=0)
print('First 3 predictions: ', y_train_pred[:3])



# トレーニングの正解率を出力する
train_acc = np.sum(y_train == y_train_pred, axis=0) / X_train.shape[0]
print('Training accuracy: %.2f%%' % (train_acc*100))

# テストの正解率を出力する
y_test_pred 0 model.predict_classes(X_test, verbose=0)
train_acc = np.sum(y_test == y_test_pred, axis=0) / X_test.shape[0]
print('Test accuracy: %.2f%%' % (test_acc*100))