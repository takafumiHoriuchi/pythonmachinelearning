# ベース分類器と、それらをアンサンブルした場合との、誤分類の関係を折れ線グラフで表す
from ensemble_error import *
import numpy as np

error_range = np.arange(0.0, 1.01, 0.01)
ens_errors = [ensemble_error(n_classifier=11, error=error)
              for error in error_range]

import matplotlib.pyplot as plt
plt.plot(error_range, ens_errors,
         label='Ensemble error',
         linewidth=2)
plt.plot(error_range, error_range,
         linestyle='--', label='Base error',
         linewidth=2)
plt.xlabel('Base error')
plt.ylabel('Base/Ensemble error')
plt.legend(loc='upper left')
plt.grid()
plt.show()
# ベース分類器の誤差率が、「当てずっぽう」とならない「< 0.5」の範囲では、
# アンサンブル分類器が常にベース分類器の性能を上回っている。