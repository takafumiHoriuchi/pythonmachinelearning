from housing_analysis import *
from linear_regression_gd import LinearRegressionGD

X = df[['RM']].values
y = df['MEDV'].values
from sklearn.preprocessing import StandardScaler
sc_x = StandardScaler()
sc_y = StandardScaler()
X_std = sc_x.fit_transform(X)
y_std = sc_y.fit_transform(y)
lr = LinearRegressionGD()
lr.fit(X_std, y_std)

# エポック数とコストの関係を表す折れ線グラフのプロット
plt.plot(range(1, lr.n_iter+1), lr.cost_)
plt.ylabel('SSE')
plt.xlabel('Epoch')
plt.show()

# ヘルパー関数
# トレーニングサンプルの散布図をプロットし、回帰直線を追加する
def lin_regplot(X, y, model):
    plt.scatter(X, y, c='blue')
    plt.plot(X, model.predict(X), color='red')
    return None


# 住宅価格に対する部屋数をプロット
lin_regplot(X_std, y_std, lr)
plt.xlabel('Average number of rooms [RM] (standardized)')
plt.yalbel('Price in $1000\'s [MEDV] (standardized)')
plt.show()


# 結果変数（住宅価格）を元の尺度に戻す
num_rooms_std = sc_x.transform([5.0])
price_std = lr.predict(num_rooms_std)
print("Price in $1000's: %.3f" % sc_y.inverse_transform(price_std))


# 標準化された変数では、y軸の切片は常に０になるため、切片の重みの更新は不要
print('Slope: %.3f' % lr.w_[1])
print('Intercept: %.3f' % lr.w_[0])


# scikit-learnのLinearRegressionオブジェクトを利用
from sklearn.linear_model import LinearRegression
slr = LinearRegression()
slr.fit(X, y)
print('Slope: %.3f' % slr.coef_[0])
print('Intercept: %3f' % slr.intercept_)


# sklearnのLinearRegressionオブジェクトと先の