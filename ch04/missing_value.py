# coding=utf-8
import pandas as pd
from io import StringIO

# サンプルデータを作成
csv_data = '''A,B,C,D
1.0,2.0,3.0,4.0
5.0,6.0,,8.0
10.0,11.0,12.0'''

# Python2.7を使用している場合は文字列をunicodeに変換する必要がある
csv_data = unicode(csv_data)

# サンプルデータを読み込む
df = pd.read_csv(StringIO(csv_data))

print()

print(df)
print()

print(df.isnull().sum())
print()

print(df.values)
print()

print(df.dropna(subset=['C']))
print()

############################################################

# coding=utf-8
from sklearn.preprocessing import Imputer

# 欠測値補完のインスタンスを生成（平均値補完）
imr = Imputer(missing_values='NaN', strategy='mean', axis=0)

# データを適合
imr = imr.fit(df)

# 補完を実行
imputed_data = imr.transform(df.values)

print(imputed_data)
print()

############################################################

