# DBSCAN (Density Based Spatial Clustering of Applications with Noise)

# 100個の点からなる半月状のクラスタを二組 作成 
from sklearn.datasets import make_moons
X, y = make_moons(n_samples=200,    # 生成する点の総数
                  noise=0.05,       # 追加するガウスノイズの標準偏差
                  random_state=0)
plt.scatter(X[:,0], X[:,1])
plt.show()





# try splitting two 半月s with k-means法 and 凝集型階層的クラスタリング（完全連結法）
f, (ax1, ax2) = plt.subplots(1, 2, figsize=(8,3))

# try splitting two 半月s with k-means法

km = KMeans(n_clusters=2,   # クラスタの個数
            #init='random',  # セントロイドの書記長
            #n_init=10,      # 実行回数
            #max_iter=300,   # 最大イテレーション回数
            #tol=1e-04,      # 収束と判定するための相対的な許容誤差
            random_state=0) # セントロイドの初期値に用いる乱数生成器の状態
y_km = km.fit_predict(X)

ax1.scatter(X[y_km==0, 0],  # グラフのxの値
            X[y_km==0, 1],  # グラフのyの値
            s=40,
            c='lightblue',
            marker='o',
            label='cluster 1')

ax1.scatter(X[y_km==1, 0],
            X[y_km==1, 1],
            s=40,
            c='red',
            marker='s',
            label='cluster 2')

ax1.set_title('K-means clustering')




# try splitting two 半月s with 凝集型階層的クラスタリング（完全連結法）

ac = AgglomerativeClustering(n_clusters=2,
                             affinity='euclidean',
                             linkage='complete')
y_ac = ac.fit_predict(X)
ax2.scatter(X[y_ac==0, 0],  # グラフのxの値
            X[y_ac==0, 1],  # グラフのyの値
            s=40,
            c='lightblue',
            marker='o',
            label='cluster 1')

ax2.scatter(X[y_ac==1, 0],
            X[y_ac==1, 1],
            s=40,
            c='red',
            marker='s',
            label='cluster 2')

ax2.set_title('Agglomerative clustering')

plt.legend()
plt.show()



# try splitting two 半月s with DBSCAN

from sklearn.cluster import DBSCAN
db = DBSCAN(eps=0.2,            # 半径
            min_samples=5,      # コア点の最小個数
            metric='euclidean') # 距離の計算方法
y_db = db.fit_predict(X)

plt.scatter(X[y_db==0, 0],  # グラフのxの値
            X[y_db==0, 1],  # グラフのyの値
            s=60,
            c='lightgreen',
            edgecolors='blue',
            marker='o',
            label='cluster 1')

plt.scatter(X[y_db==1, 0],
            X[y_db==1, 1],
            s=60,
            c='lightgreen',
            edgecolors='red',
            marker='s',
            label='cluster 2')

plt.set_title('DBSCAN')
plt.legend()
plt.show()