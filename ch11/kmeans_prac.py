from sklearn.datasets import make_blobs

# ランダムに150個の点を生成する
X, y = make_blobs(n_samples=150,    # 点の個数
                  n_features=2,     # 特徴量の個数（２次元）
                  centers=3,        # クラスタの個数
                  cluster_std=0.5,  # クラスタ内の標準偏差
                  shuffle=True,
                  random_state=0)   # 乱数生成器の状態を指定


# ２次元の散布図にまとめる
import matplotlib.pyplot as plt

plt.scatter(X[:,0], X[:,1], c='white', marker='o', s=50)
plt.grid()
plt.show()


from sklearn.cluster import KMeans
km = KMeans(n_clusters=3,   # クラスタの個数
            init='random',  # セントロイドの書記長
            n_init=10,      # 実行回数
            max_iter=300,   # 最大イテレーション回数
            tol=1e-04,      # 収束と判定するための相対的な許容誤差
            random_state=0) # セントロイドの初期値に用いる乱数生成器の状態
y_km = km.fit_predict(X)


# クラスタとセントロイドをプロットする

plt.scatter(X[y_km==0, 0],  # グラフのxの値
            X[y_km==0, 1],  # グラフのyの値
            s=50,
            c='lightgreen',
            marker='s',
            label='cluster 1')

plt.scatter(X[y_km==1, 0],
            X[y_km==1, 1],
            s=50,
            c='orange',
            marker='o',
            label='cluster 2')

plt.scatter(X[y_km==2, 0],
            X[y_km==2, 1],
            s=50,
            c='lightblue',
            marker='v',
            label='cluster 3')

plt.scatter(km.cluster_centers_[:,0],
            km.cluster_centers_[:,1],
            s=250,
            c='red',
            marker='*',
            label='centroids')

plt.legend()
plt.grid()
plt.show()