# coding=utf-8
# データセットを読み込み、トレーニングデータセットとテストデータセットに分割する

import pandas as pd
df = pd.read_csv('./wdbc.data', header=None)

from sklearn.preprocessing import LabelEncoder
# 30個の特徴量をXに格納する
X = df.loc[:, 2:].values
# M(malignant)とB(benign)の列をyに格納する
y = df.loc[:, 1].values
le = LabelEncoder()
# M,Bを数値に変換する
y = le.fit_transform(y)

from sklearn.cross_validation import train_test_split
X_train, X_test, y_train, y_test = \
train_test_split(X, y, test_size=0.20, random_state=1)

from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import Pipeline
# 連結する処理としてスケーリング、主成分分析、ロジスティック回帰を指定
pipe_lr = Pipeline([('scl', StandardScaler()), ('pca', PCA(n_components=2)),
                    ('clf', LogisticRegression(random_state=1))])
pipe_lr.fit(X_train, y_train)
print('Test Accuracy: %.3f' % pipe_lr.score(X_test, y_test))
